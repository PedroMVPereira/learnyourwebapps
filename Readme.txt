# Git Repository to learn and develop Web Applications using JavaScript #

This is a incremental learning setup, as we progress , the dificulty will increase and
the more detailed implementations will be focused on.

For now, we will conduct a simple "workshop" that will allow us to get to know the
language and the correct way to optimize our code knowing our main target : WebApps.


Steps :

Step 1 : Install Nodejs.
Step 2 : Install Javascripting.
Step 3 : Follow the Files System Folders that is available in the reporsitory for each exercice.
Step 4 : Finish all entries of JavaScripting

Good Luck!!


Guideline: 
****************************************************************

Use any JavaScript IDE, recommended : Visual Studio / Visual Code / IntelliJ / Eclipse


Make sure Node.js is installed on your computer.

Install it from nodejs.org

On Windows and using v4 or v5 of Node.js? Make sure you are using at least 5.1.0, which provides a fix for a bug on Windows where you can't choose items in the menu.

Install javascripting with npm

Open your terminal and run this command:

npm install --global javascripting
The --global option installs this module globally so that you can run it as a command in your terminal.

Having issues with installation?

If you get an EACCESS error, the simplest way to fix this is to rerun the command, prefixed with sudo:

sudo npm install --global javascripting